
@login
Feature: login
  User want to login using valid credential
 

  @login_positive
  Scenario Outline: As a user i should be able to login with valid credential
  	Given I open browser
    Given I navigate to url <url>
    Given I click nav link login
    Given I enter username for login <userName> 
    Given I enter password for login <password>
    When I click button login 
    Then I should redirected to home page screen
    Then I close browser
    
    Examples:
    |							url 						|	userName |	password	|
    |	https://www.demoblaze.com	 	|	 23user	 |  password	|