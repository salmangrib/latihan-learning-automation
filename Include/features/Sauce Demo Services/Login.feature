@login
Feature: Login
  User want to login

Background: open browser

  @Login @negative
  Scenario: As a user i shouldnt be able to login using invalid credential
    Then User input invalid username "salah"
    Then User input valid password "password"
    Then User click button login

  @Login @positive
  Scenario: User want to login using valid credential
    Then User input valid username "standard_user"
    Then User input valid password "secret_sauce"
    Then User click button login and success

   