import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


/**
 * The step definitions below match with Katalon sample Gherkin steps
 */

class login {

	@Then("User input invalid username {string}")
	public void user_input_invalid_username(String username) {
		WebUI.setText(findTestObject('Sauce Demo Services/Login Page/field_username'), 'salah')
	}

	@Then("User input invalid password {string}")
	public void user_input_invalid_password(String password) {
		WebUI.setEncryptedText(findTestObject('Sauce Demo Services/Login Page/field_password'), 'aeHFOx8jV/A=')
	}

	@Then("User click button login")
	public void user_click_button_login() {
		WebUI.click(findTestObject('Sauce Demo Services/Login Page/btn_login'))
	}

	@Then("User input valid username {string}")
	public void user_input_valid_username(String username) {
		WebUI.setText(findTestObject('Sauce Demo Services/Login Page/field_username'), 'standard_user')
	}

	@Then("User input valid password {string}")
	public void user_input_valid_password(String password) {
		WebUI.setEncryptedText(findTestObject('Object Repository/Sauce Demo Services/Login Page/field_password'), 'qcu24s4901FyWDTwXGr6XA==')
	}

	@Then("User click button login and success")
	public void user_click_button_login_and_success() {
		WebUI.click(findTestObject('Object Repository/Sauce Demo Services/Login Page/btn_login'))
	}
}