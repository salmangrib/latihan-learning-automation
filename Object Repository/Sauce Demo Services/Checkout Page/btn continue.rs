<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn continue</name>
   <tag></tag>
   <elementGuidId>7e346932-4d3d-4def-bab8-21059be210d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#continue</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='continue']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e581eeed-789d-47b3-9d0a-2e2e8f0cf47f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>d151e51d-9f07-4161-82eb-76711542d2b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button btn btn_primary cart_button btn_action</value>
      <webElementGuid>330d2b86-cb24-471e-a8d5-1b32cce78187</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>403df1d7-5db3-4366-8767-04be05e9a7c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>2120716a-7914-41fb-b657-796d751aa7de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>af4cfe2b-222e-4007-9c46-25d158990ce2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>5fc2b935-aab4-4e8c-a4b3-450fe98d2001</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;continue&quot;)</value>
      <webElementGuid>5b28e290-b7f1-4110-9221-87dc06d1a2f3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='continue']</value>
      <webElementGuid>05a74bb7-cc16-48d4-8819-12279efd20c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout_info_container']/div/form/div[2]/input</value>
      <webElementGuid>5cf7da93-49c1-49de-bba6-89e730a2830c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>38fa128b-e329-4bf7-90c2-cf46c7eeb7ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'continue' and @name = 'continue']</value>
      <webElementGuid>618f361b-3609-42da-90bb-a85eabc2b508</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
