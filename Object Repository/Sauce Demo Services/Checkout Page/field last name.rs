<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field last name</name>
   <tag></tag>
   <elementGuidId>863ea30d-e204-4f7a-9079-bace42e18f34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#last-name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='last-name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d5d017d9-6ff3-4201-a8d8-67ccd104c1d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input_error form_input</value>
      <webElementGuid>8781af4f-e02a-43fc-a986-9ad2afc5740b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Last Name</value>
      <webElementGuid>e127f4cc-6fd9-4852-8c1e-64b9b1a32a7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>ed2a90d0-cf8c-4c75-8d40-e66351f2ccb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>lastName</value>
      <webElementGuid>cc91eee6-b650-46ed-9c60-dd1e9d0e6bed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>last-name</value>
      <webElementGuid>8ec63a92-6a66-4529-843a-f9efe59b191b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>lastName</value>
      <webElementGuid>1790e7e7-670f-4230-914b-902e7d7bf0c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>3bb77d5e-830e-4d98-96e3-5e245c5ebb0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>7d1c1e83-f211-4f08-a567-dbdf80aefe86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;last-name&quot;)</value>
      <webElementGuid>91115606-ca7e-42a2-804d-ea45cc99f0ea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='last-name']</value>
      <webElementGuid>ec1b25e9-9411-4c3f-8696-d6e89f2d7713</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout_info_container']/div/form/div/div[2]/input</value>
      <webElementGuid>3f7cf78f-9b05-49e9-a10b-b41c94c190c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/input</value>
      <webElementGuid>f35f3d0f-7d1d-4611-acff-93a039152058</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Last Name' and @type = 'text' and @id = 'last-name' and @name = 'lastName']</value>
      <webElementGuid>abf2b9d7-cbc5-4025-8cca-830d813e7427</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
